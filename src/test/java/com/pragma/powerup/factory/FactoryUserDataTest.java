package com.pragma.powerup.factory;

import com.pragma.powerup.domain.model.RoleModel;
import com.pragma.powerup.domain.model.UserModel;

public class FactoryUserDataTest {

    public static UserModel getUserModel(){

        UserModel userModel = new UserModel(null,"Javier","Lopez",
                "75554433", "99898773","jav@gmail.com",
                "123",new RoleModel(2L,null,null));

        return userModel;
    }
}
