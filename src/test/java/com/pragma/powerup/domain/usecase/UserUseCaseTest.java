package com.pragma.powerup.domain.usecase;

import com.pragma.powerup.domain.model.UserModel;
import com.pragma.powerup.domain.spi.IUserPersistencePort;
import com.pragma.powerup.factory.FactoryUserDataTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class UserUseCaseTest {

    @InjectMocks
    UserUseCase userUseCase;
    @Mock
    IUserPersistencePort userPersistencePort;

    @Test
    void saveUser() {
        //Given
        UserModel userModel = FactoryUserDataTest.getUserModel();
        //When
        userUseCase.saveUser(userModel);
        //Then
        verify(userPersistencePort).saveUser(any(UserModel.class));
    }
}