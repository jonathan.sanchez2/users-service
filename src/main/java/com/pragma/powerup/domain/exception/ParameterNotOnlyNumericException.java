package com.pragma.powerup.domain.exception;

public class ParameterNotOnlyNumericException extends RuntimeException {

    public ParameterNotOnlyNumericException() {
        super();
    }

}
