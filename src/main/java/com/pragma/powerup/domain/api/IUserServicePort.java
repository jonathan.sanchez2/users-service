package com.pragma.powerup.domain.api;

import com.pragma.powerup.domain.model.UserModel;

public interface IUserServicePort {

    void saveUser(UserModel userModel);

    UserModel getUserByEmail(String email);

}