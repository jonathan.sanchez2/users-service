package com.pragma.powerup.domain.model;

import com.pragma.powerup.domain.exception.EmailFormatException;
import com.pragma.powerup.domain.exception.ParameterLengthException;
import com.pragma.powerup.domain.exception.ParameterTypeException;

public class UserModel {
    private Long id;
    private String name;
    private String surname;
    private String identificationDocument;
    private String cellphone;
    private String email;
    private String password;
    private RoleModel role;

    public UserModel(Long id, String name, String surname, String identificationDocument, String cellphone, String email, String password, RoleModel role) {

        if(!email.matches("^(.+)@(.+)$")){

            throw new EmailFormatException();
        }

        if(cellphone.length()>13){

            throw new ParameterLengthException();
        }

        if(!identificationDocument.matches("[0-9]+")){

            throw new ParameterTypeException();
        }

        this.id = id;
        this.name = name;
        this.surname = surname;
        this.identificationDocument = identificationDocument;
        this.cellphone = cellphone;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getIdentificationDocument() {
        return identificationDocument;
    }

    public void setIdentificationDocument(String identificationDocument) {
        this.identificationDocument = identificationDocument;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleModel getRole() {
        return role;
    }

    public void setRole(RoleModel role) {
        this.role = role;
    }
}
