package com.pragma.powerup.infrastructure.input.rest;

import com.pragma.powerup.application.dto.request.UserRequestDto;
import com.pragma.powerup.application.handler.IUserHandler;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
@Transactional
public class UserRestController {

    private final IUserHandler userHandler;

    @Operation(summary = "Add a new user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "User created", content = @Content),
            @ApiResponse(responseCode = "409", description = "User already exists", content = @Content)
    })

    @PostMapping("/owner")
    public ResponseEntity<Void> saveOwner(@RequestBody UserRequestDto userRequestDto) {
        userHandler.saveUser(userRequestDto,2L);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
    //@PreAuthorize("hasRole('Owner')")
    @PostMapping("/employee")
    public ResponseEntity<Void> saveEmployee(@RequestBody UserRequestDto userRequestDto) {
        userHandler.saveUser(userRequestDto,3L);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
    //@PreAuthorize("hasRole('Client')")
    @PostMapping("/client")
    public ResponseEntity<Void> saveClient(@RequestBody UserRequestDto userRequestDto) {
        userHandler.saveUser(userRequestDto,4L);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    /*@Operation(summary = "Get all objects")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "All objects returned",
                    content = @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = UserResponseDto.class)))),
            @ApiResponse(responseCode = "404", description = "No data found", content = @Content)
    })
    @GetMapping("/")
    public ResponseEntity<List<UserResponseDto>> getAllObjects() {
        return ResponseEntity.ok(objectHandler.getAllObjects());
    }*/

}