package com.pragma.powerup.infrastructure.security;

import com.pragma.powerup.domain.model.UserModel;
import com.pragma.powerup.domain.spi.IUserPersistencePort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserDetailsService {

    @Autowired
    private IUserPersistencePort userPersistencePort;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        UserModel userModel = userPersistencePort.getUserByEmail(email);

        if(userModel == null) {
            throw new UsernameNotFoundException(String.format("Usuario no existe: %s", email));
        }

        List<GrantedAuthority> roles = new ArrayList<>();

            roles.add(new SimpleGrantedAuthority(userModel.getRole().getName()));

        UserDetails ud = new User(userModel.getEmail(), userModel.getPassword(), true, true, true, true, roles);

        return ud;
    }

}
