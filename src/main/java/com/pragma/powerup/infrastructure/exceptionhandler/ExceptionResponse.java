package com.pragma.powerup.infrastructure.exceptionhandler;

public enum ExceptionResponse {
    NO_DATA_FOUND("No data found for the requested petition"),
    EMAIL_FORMAT("El valor ingresado no es email valido"),
    PARAMETER_LENGTH("El valor ingresado no puede tener mas de 13 caracteres"),
    PARAMETER_TYPE("El valor ingresado no es numerico");
    private final String message;

    ExceptionResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}