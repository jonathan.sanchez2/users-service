package com.pragma.powerup.application.dto.request;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
@NonNull
public class UserRequestDto {
    private String name;
    private String surname;
    private String identificationDocument;
    private String cellphone;
    private String email;
    private String password;
}
