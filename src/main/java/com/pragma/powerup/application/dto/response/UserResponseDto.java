package com.pragma.powerup.application.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserResponseDto {
    private String name;
    private String surname;
    private String identificationDocument;
    private String cellphone;
    private String email;
    private String password;
    private RoleResponseDto role;
}
